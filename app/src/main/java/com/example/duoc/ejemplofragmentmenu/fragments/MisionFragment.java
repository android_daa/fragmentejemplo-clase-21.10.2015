package com.example.duoc.ejemplofragmentmenu.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.duoc.ejemplofragmentmenu.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MisionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MisionFragment extends Fragment {

    public static MisionFragment newInstance() {
        MisionFragment fragment = new MisionFragment();
        return fragment;
    }

    public MisionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_mision, container, false);
    }


}
