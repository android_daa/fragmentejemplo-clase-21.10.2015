package com.example.duoc.ejemplofragmentmenu;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.duoc.ejemplofragmentmenu.fragments.ContactoFragment;
import com.example.duoc.ejemplofragmentmenu.fragments.HomeFragment;
import com.example.duoc.ejemplofragmentmenu.fragments.MisionFragment;
import com.example.duoc.ejemplofragmentmenu.fragments.VisionFragment;


public class MainActivity extends ActionBarActivity {


    private HomeFragment homeFragment;
    private VisionFragment visionFragment;
    private MisionFragment misionFragment;
    private ContactoFragment contactoFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        homeFragment = HomeFragment.newInstance();
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.contentFragment, homeFragment);
        ft.commit();

    }

    public void onClickInicio(View v){
        if(homeFragment == null){
            homeFragment = HomeFragment.newInstance();
        }

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.contentFragment, homeFragment);
        ft.commit();
    }

    public void onClickVision(View v){

        if(visionFragment == null){
            visionFragment = VisionFragment.newInstance();
        }

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.contentFragment, visionFragment);
        ft.commit();
    }

    public void onClickMision(View v){
        if(misionFragment == null){
            misionFragment = MisionFragment.newInstance();
        }

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.contentFragment, misionFragment);
        ft.commit();
    }

    public void onClickContacto(View v){
        if(contactoFragment == null){
            contactoFragment = ContactoFragment.newInstance();
        }

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.contentFragment, contactoFragment);
        ft.commit();
    }
}
