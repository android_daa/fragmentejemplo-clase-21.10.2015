package com.example.duoc.ejemplofragmentmenu.fragments;


import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.duoc.ejemplofragmentmenu.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link VisionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VisionFragment extends Fragment {

    public static VisionFragment newInstance() {
        VisionFragment fragment = new VisionFragment();
        return fragment;
    }

    public VisionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_vision, container, false);
    }


}
